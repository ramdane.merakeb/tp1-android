package com.example.tp2;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class WineActivity extends AppCompatActivity {

    private WineDbHelper databaseVins ;
    private Wine wine ;
    private Cursor cursor;
    private EditText wineName ;
    private EditText WineRegion ;
    private EditText Loc ;
    private EditText Climate ;
    private EditText PlantedArea ;
    private Button button ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wine);

        //on cree un instance de la BD
        databaseVins = new WineDbHelper(this);

        /// lier les views au vraiables
        wineName = (EditText) findViewById(R.id.wineName) ;
        WineRegion = (EditText) findViewById(R.id.editWineRegion) ;
        Loc = (EditText) findViewById(R.id.editLoc) ;
        Climate = (EditText) findViewById(R.id.editClimate) ;
        PlantedArea = (EditText) findViewById(R.id.editPlantedArea) ;
        button = (Button) findViewById(R.id.button);

        // on recupere le Wine selectionner sur la listView
        Bundle extras = getIntent().getExtras();
        wine = (Wine) extras.get("wineSelected");

        // on verifie si le wine n'est pas null est on affecte les données
        if(wine!=null) {
            wineName.setText(wine.getTitle());
            WineRegion.setText(wine.getRegion());
            Loc.setText(wine.getLocalization());
            Climate.setText(wine.getClimate());
            PlantedArea.setText(wine.getPlantedArea());
        }

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Vérification et mis a jour
                if(wine!=null) {
                    if(!wineName.getText().toString().isEmpty()) {
                        wine.setTitle(wineName.getText().toString());
                        wine.setRegion(WineRegion.getText().toString());
                        wine.setLocalization(Loc.getText().toString());
                        wine.setClimate(Climate.getText().toString());
                        wine.setPlantedArea(PlantedArea.getText().toString());

                        //verification si il existe un vin portant le meme nom et region sur la base de données
                        try {
                                databaseVins.updateWine(wine);
                                Toast.makeText(getApplicationContext(), "la modification a bien été effectuée ", Toast.LENGTH_LONG).show();


                        }catch (SQLiteConstraintException e){
                            AlertDialog.Builder builder = new AlertDialog.Builder(WineActivity.this);
                            builder.setTitle("Modification impossible.");
                            builder.setMessage("Un vin portant le meme nom et region existe déjà dans la base de données");
                            builder.setCancelable(true);
                            builder.show();
                            return;
                        }

                    }else{

                        AlertDialog.Builder builder = new AlertDialog.Builder(WineActivity.this);
                        builder.setTitle("Sauvegarde impossible.");
                        builder.setMessage("Le nom du vin doit etre non vide.");
                        builder.setCancelable(true);
                        builder.show();
                        return;
                    }
                }else{
                    //Vérification et Ajout
                    if(wineName.getText().toString().isEmpty()){
                        AlertDialog.Builder builder = new AlertDialog.Builder(WineActivity.this);
                        builder.setTitle("Sauvegarde impossible.");
                        builder.setMessage("Le nom du vin doit etre non vide.");
                        builder.setCancelable(true);
                        builder.show();
                        return;
                    }else{

                        wine = new Wine(wineName.getText().toString(), WineRegion.getText().toString(),Loc.getText().toString(), Climate.getText().toString(), PlantedArea.getText().toString());
                        if(!databaseVins.addWine(wine)){

                            AlertDialog.Builder builder = new AlertDialog.Builder(WineActivity.this);
                            builder.setTitle("Ajout impossible.");
                            builder.setMessage("Un vin portant le meme nom existe déjà dans la base de données");
                            builder.setCancelable(true);
                            builder.show();
                            return;

                        }else{
                            Toast.makeText(getApplicationContext(), "la modification a bien été effectuée ", Toast.LENGTH_LONG).show();

                        }

                    }

                }



            }
        });




    }
}
