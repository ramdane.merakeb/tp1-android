package com.example.tp1;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.ViewGroup;

import static com.example.tp1.R.id.*;

public class Main2Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        final RecyclerView rv = (RecyclerView) findViewById(R.id.recycler_view);

        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        rv.setAdapter(new IconicAdapter());
    }


    class IconicAdapter extends RecyclerView.Adapter<RowHolder> {

        @Override
        public RowHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return(new RowHolder(getLayoutInflater().inflate(R.layout.row, parent, false)));
        }

        @Override
        //On lie notre recycler view avec notre animalList pour recuperer le nom de l'animal
        public void onBindViewHolder(RowHolder holder, int position) {
            holder.bindModel(AnimalList.getNameArray()[position]);
        }

        @Override
        public int getItemCount() {
            return(AnimalList.getNameArray().length);
        }
    }

}
