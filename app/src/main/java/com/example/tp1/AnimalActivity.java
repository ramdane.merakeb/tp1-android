package com.example.tp1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class AnimalActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animal);

        Intent intent = getIntent();
        String animalItem = intent.getStringExtra("animalName");
        final Animal animal = AnimalList.getAnimal(animalItem);

        //recuperation de nom et insertion dans l'interface
        TextView nom = findViewById(R.id.nom);
        nom.setText(animalItem);

        //recuperation de l'image et insertion
        ImageView icon = findViewById(R.id.imageView);
        Resources res = getResources();
        int resID = res.getIdentifier(animal.getImgFile() , "drawable", getPackageName());
        Drawable drawable = res.getDrawable(resID );
        icon.setImageDrawable(drawable );


        //recuperation de l'esperance
        TextView esperance = findViewById(R.id.esperance);
        esperance.setText(animal.getStrHightestLifespan());

        //recuperation de gestation
        TextView gestation = findViewById(R.id.gestation);
        gestation.setText(animal.getStrGestationPeriod());

        //recuperation de poid de naissance
        TextView poidNaissance = findViewById(R.id.poidN);
        poidNaissance.setText(animal.getStrBirthWeight());

        //recuperation de poid adulte
        TextView poidAdulte = findViewById(R.id.poidA);
        poidAdulte.setText(animal.getStrAdultWeight());


        //recuperation de la conservation
        final EditText conservation = findViewById(R.id.conservation);
        conservation.setText(animal.getConservationStatus());


        Button button = (Button) findViewById(R.id.sauvegarder) ;
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animal.setConservationStatus(conservation.getText().toString());

                //TextView Stat = (TextView) findViewById(R.id.stat) ;
                conservation.setText(animal.getConservationStatus());

            }
        });






    }
}
