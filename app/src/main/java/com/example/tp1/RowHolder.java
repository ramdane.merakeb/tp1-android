package com.example.tp1;


import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

class RowHolder extends RecyclerView.ViewHolder
        implements View.OnClickListener {

    TextView label;
    ImageView icon;

    RowHolder(View row) {
        super(row);

        label=(TextView) row.findViewById(R.id.label);
        icon=(ImageView )row.findViewById(R.id.imageViewRow);
        this.itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v){

        final String item = label.getText().toString();
        Intent intent = new Intent(this.itemView.getContext(), AnimalActivity.class);
        intent.putExtra("animalName", item);
        this.itemView.getContext().startActivity(intent);

    }
    void bindModel(String item) {
        label.setText(item);
        Animal animal = AnimalList.getAnimal(item);
        icon.setImageResource(this.itemView.getContext().getResources().getIdentifier(animal.getImgFile(), "drawable",this.itemView.getContext().getPackageName()));

    }


}
